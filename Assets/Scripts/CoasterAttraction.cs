﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class Wagon
{
    public Vector3 position;
    public float t;
    public int segment;
    public float speed;
    public bool active;

    public CurveElement previousElement;
    public CurveElement nextElement;
    public CurveElement prevPreviousElement;
    public CurveElement nextNextElement;

}

public class CoasterAttraction : MonoBehaviour
{

    private List<CurveElement> curve;
    private Track track;

    private Wagon wagon;

    public GameObject wagonPrefab;
    public GameObject playerController;

    public void addElement(int index)
    {
        if (index < 5)
        {
            Debug.Log("Error, can't add element");
            return;
        }

        int c = (index + curve.Count - 1) % curve.Count;
        CurveElement previousElement = curve[c];
        CurveElement nextElement = curve[(c + 1) % curve.Count];
        CurveElement prevPreviousElement = curve[(c + curve.Count - 1) % curve.Count];
        CurveElement nextNextElement = curve[(c + 2) % curve.Count];

        Debug.Log("Prev " + previousElement.position + " " + previousElement.forward);
        Debug.Log("Next " + nextElement.position + " " + nextElement.forward);


        CurveElement newElement = new CurveElement();
        newElement.position = Track.GetBezierPosition(0.5f, previousElement, nextElement);
        newElement.forward = Track.GetBezierForward(0.5f, previousElement, nextElement);
        newElement.left = Track.GetBezierLeft(0.5f, newElement.forward, previousElement, nextElement, prevPreviousElement, nextNextElement);
        Debug.Log("New " + newElement.position + " " + newElement.forward);
        curve.Insert(index, newElement);
        track.UpdateAll();
        Debug.Log("add element at position " + index);
    }

    public void deleteElement(int index)
    {
        if (index < 5)
        {
            Debug.Log("Error, can't delete element");
            return;
        }
        curve.RemoveAt(index);
        track.UpdateAll();
        Debug.Log("delete element from position " + index);
    }

    public void moveElement(int index, Vector3 position)
    {
        if (index < 5)
        {
            Debug.Log("Error, can't move this element");
            return;
        }
        curve[index].position = position;
        track.UpdatePositionsAround(index);
        //Debug.Log("move element "+index);
    }

    public void changeElementDirection(int index, Vector3 direction)
    {
        if (index < 5)
        {
            Debug.Log("Error, can't change this element's direction");
            return;
        }
        curve[index].forward = direction;
        track.UpdatePositionsAround(index);
        Debug.Log("change direction element " + index);
    }

    public void changeElementLeft(int index, Vector3 left)
    {
        if (index < 5)
        {
            Debug.Log("Error, can't this element's left vector");
            return;
        }
        curve[index].left = Vector3.Normalize(Vector3.ProjectOnPlane(left, curve[index].forward));
        track.UpdatePositionsAround(index);
        Debug.Log("change left element " + index);
    }

    public void resetWagon()
    {
        wagon.t = 0f;
        wagon.segment = 0;
        wagon.speed = 0f;
        wagon.active = false;


        wagon.previousElement = curve[wagon.segment];
        wagon.nextElement = curve[(wagon.segment + 1) % curve.Count];
        wagon.prevPreviousElement = curve[(wagon.segment + curve.Count - 1) % curve.Count];
        wagon.nextNextElement = curve[(wagon.segment + 2) % curve.Count];
    }

    public void runWagon()
    {
        wagon.active = true;
    }

    // Use this for initialization
    void Start()
    {
        wagonPrefab = Instantiate(wagonPrefab);



        curve = GetComponent<Track>().curve;
        track = GetComponent<Track>();


        t = 0.0f;
        td = 0.0f;




        wagon = new Wagon();
        wagon.t = 0f;
        wagon.segment = 0;
        wagon.speed = 0f;
        wagon.active = false;


        wagon.previousElement = curve[wagon.segment];
        wagon.nextElement = curve[(wagon.segment + 1) % curve.Count];
        wagon.prevPreviousElement = curve[(wagon.segment + curve.Count - 1) % curve.Count];
        wagon.nextNextElement = curve[(wagon.segment + 2) % curve.Count];
    }

    public void climbIn()
    {
        playerController.transform.parent = wagonPrefab.transform;
        playerController.transform.localPosition = new Vector3(0, -1.8f, 0);
        playerController.GetComponent<CharacterController>().enabled = false;
        playerController.transform.localEulerAngles =
            new Vector3(playerController.transform.localEulerAngles.x,
            0f,
            playerController.transform.localEulerAngles.z);
        playerController.GetComponent<PlayerMovement>().playerRotSpeed = 0f;
    }

    public Wagon moveWagon(Wagon wag, float step)
    {
        Wagon w = CloneWagon(wag);
        w.t += step;

        while (w.t > 1.0f)
        {
            w.t -= 1.0f;
            w.segment = (w.segment + 1) % curve.Count;
            w.previousElement = curve[w.segment];
            w.nextElement = curve[(w.segment + 1) % curve.Count];
            w.prevPreviousElement = curve[(w.segment + curve.Count - 1) % curve.Count];
            w.nextNextElement = curve[(w.segment + 2) % curve.Count];
        }

        while (w.t < 0.0f)
        {
            w.t += 1.0f;
            w.segment = (w.segment + curve.Count - 1) % curve.Count;
            w.previousElement = curve[w.segment];
            w.nextElement = curve[(w.segment + 1) % curve.Count];
            w.prevPreviousElement = curve[(w.segment + curve.Count - 1) % curve.Count];
            w.nextNextElement = curve[(w.segment + 2) % curve.Count];
        }

        return w;
    }

    public float distTwoWagons(Wagon w1param, Wagon w2param)
    {
        Wagon w1 = CloneWagon(w1param);
        Wagon w2 = CloneWagon(w2param);

        float dist = 0.0f;
        while (w1.segment != w2.segment)
        {
            dist += 1.0f - w1.t;
            w1.t = 0.0f;
            w1.segment = (w1.segment + 1) % curve.Count;
        }
        dist += w2.t - w1.t;
        return dist;
    }

    public Wagon avgWagons(Wagon w1, Wagon w2)
    {
        Wagon w = CloneWagon(w1);
        w = moveWagon(w, distTwoWagons(w1, w2) * 0.5f);

        return w;
    }


    public static Wagon CloneWagon(Wagon source)
    {
        Wagon w = new Wagon();

        w.position = source.position;
        w.t = source.t;
        w.segment = source.segment;
        w.speed = source.speed;
        w.active = source.active;

        w.previousElement = source.previousElement;
        w.nextElement = source.nextElement;
        w.prevPreviousElement = source.prevPreviousElement;
        w.nextNextElement = source.nextNextElement;

        return w;
    }

    private float t;
    private float td;

    // Update is called once per frame
    void Update()
    {


        //float delta = Time.deltaTime;
        float delta = 0.013f;

        /*CurveElement previousElement = curve[wagon.segment];
        CurveElement nextElement = curve[(wagon.segment + 1) % curve.Count];
        CurveElement prevPreviousElement = curve[(wagon.segment + curve.Count - 1) % curve.Count];
        CurveElement nextNextElement = curve[(wagon.segment + 2) % curve.Count];*/

        Vector3 pos = Track.GetBezierPosition(wagon.t, wagon.previousElement, wagon.nextElement);
        wagon.position = pos;

        Vector3 localForward = Track.GetBezierForward(wagon.t, wagon.previousElement, wagon.nextElement);

        float acceleration = 0.0f;

        //if(wagon.active)
        {
            if (Vector3.Dot(Vector3.Normalize(localForward), new Vector3(0f, -1f, 0f)) > 0.1f)
            { // on ne pousse pas en descente
                acceleration = 0.0f;
            }
            else if (wagon.speed <= 0.0f)
            { // on recule pour reprendre de l'élan
                acceleration = 0.05f;
            }
            else if (wagon.speed <= -0.005f)
            { // on recule pour reprendre de l'élan
                acceleration = -0.1f;
            }
            else
            {
                acceleration += 0.16f / Mathf.Sqrt(wagon.speed + 1.0f)/* - Mathf.Clamp(0.8f * Vector3.Dot(Vector3.Normalize(localForward), new Vector3(0f, -1f, 0f))   , -0.1f, 0.0f)*/; // motor
                                                                                                                                                                                         // acceleration += 0.86f;
            }
            //acceleration += 0.18f; // motor
        }

        acceleration += 0.31f * Vector3.Dot(Vector3.Normalize(localForward), new Vector3(0f, -1f, 0f)); // gravity
        acceleration -= wagon.speed * wagon.speed * 1.5f; // frottements de l'air

        acceleration *= delta;

        wagon.speed += acceleration;
        wagon.speed *= Mathf.Exp(-0.5f * delta); // frottements avec les rails


        //Debug.Log("speed = "+wagon.speed);
        //wagon.speed *= 0.99972576337f;

        Vector3 newPos;
        newPos.x = wagon.position.x;
        newPos.y = wagon.position.y;
        newPos.z = wagon.position.z;

        /*
                float step = 0.001f * Mathf.Sign(wagon.speed);
                int safetyI = 0;
                float distance = 99999999f;
                do
                {
                    if (safetyI > 50000)
                    {
                        Debug.Log("safety");
                        break;
                    }
                    if(wagon.speed < 0.0001f && wagon.speed > -0.0001f)
                    {
                        break;
                    }


                    wagon.t += step;


                    while (wagon.t > 1.0f)
                    {
                        wagon.t -= 1.0f;
                        wagon.segment = (wagon.segment + 1) % curve.Count;
                        previousElement = curve[wagon.segment];
                        nextElement = curve[(wagon.segment + 1) % curve.Count];
                        prevPreviousElement = curve[(wagon.segment + curve.Count - 1) % curve.Count];
                        nextNextElement = curve[(wagon.segment + 2) % curve.Count];
                    }

                    while (wagon.t < 0.0f)
                    {
                        wagon.t += 1.0f;
                        wagon.segment = (wagon.segment + curve.Count - 1) % curve.Count;
                        previousElement = curve[wagon.segment];
                        nextElement = curve[(wagon.segment + 1) % curve.Count];
                        prevPreviousElement = curve[(wagon.segment + curve.Count - 1) % curve.Count];
                        nextNextElement = curve[(wagon.segment + 2) % curve.Count];
                    }

                    newPos = Track.GetBezierPosition(wagon.t, previousElement, nextElement);

                    distance = Mathf.Abs(Vector3.Magnitude(newPos - wagon.position));

                    ++safetyI;
                } while (Mathf.Abs(wagon.speed) * delta > distance);
                */

        int stepNumber = 8;
        float stepSize = wagon.speed / (float)stepNumber;

        int safetyI = 0;

        for (int step = 0; step < stepNumber; step++)
        {
            if (safetyI >= 5000)
            {
                break;
            }

            Wagon minWagon = CloneWagon(wagon);
            Wagon maxWagon = CloneWagon(wagon);

            if (wagon.speed > 0.0f)
            {
                for (; safetyI <= 5000; ++safetyI)
                {
                    maxWagon = moveWagon(maxWagon, 0.1f);

                    Vector3 Mpos = Track.GetBezierPosition(maxWagon.t, maxWagon.previousElement, maxWagon.nextElement);
                    maxWagon.position = Mpos;
                    Vector3 currentPos = wagon.position;
                    float dist = Vector3.Magnitude(currentPos - Mpos);

                    if (dist > Mathf.Abs(stepSize))
                    {
                        break;
                    }
                }
            }
            if (wagon.speed < 0.0f)
            {
                for (; safetyI <= 5000; ++safetyI)
                {
                    minWagon = moveWagon(minWagon, -0.1f);

                    Vector3 Mpos = Track.GetBezierPosition(minWagon.t, minWagon.previousElement, minWagon.nextElement);
                    minWagon.position = Mpos;
                    Vector3 currentPos = wagon.position;
                    float dist = Vector3.Magnitude(currentPos - Mpos);

                    if (dist > Mathf.Abs(stepSize))
                    {
                        break;
                    }
                }
            }

            //Debug.Log("min : " + minWagon.segment + ", " + minWagon.t);
            //Debug.Log("max : " + maxWagon.segment + ", " + maxWagon.t);

            //Debug.Log("Start finding best :");
            for (; safetyI <= 5000; ++safetyI)
            {
                //Debug.Log("min wagon : t="+minWagon.t+" , segment"+minWagon.segment+" : "+minWagon.position);
                //Debug.Log("max wagon : t="+maxWagon.t+" , segment"+maxWagon.segment+" : "+maxWagon.position);

                Wagon middleWagon = avgWagons(minWagon, maxWagon);

                Vector3 Mpos = Track.GetBezierPosition(middleWagon.t, middleWagon.previousElement, middleWagon.nextElement);
                middleWagon.position = Mpos;
                //Debug.Log("middle wagon : t="+middleWagon.t+" , segment"+middleWagon.segment+" : "+middleWagon.position);
                Vector3 currentPos = wagon.position;
                float dist = Vector3.Magnitude(currentPos - Mpos);
                //Debug.Log("new dist : "+dist);
                //Debug.Log("step size : "+stepSize);

                if (dist > Mathf.Abs(stepSize))
                {
                    if (wagon.speed > 0.0f)
                    {
                        maxWagon = CloneWagon(middleWagon);
                    }
                    if (wagon.speed < 0.0f)
                    {
                        minWagon = CloneWagon(middleWagon);
                    }
                }
                if (dist < Mathf.Abs(stepSize))
                {
                    if (wagon.speed > 0.0f)
                    {
                        minWagon = CloneWagon(middleWagon);
                    }
                    if (wagon.speed < 0.0f)
                    {
                        maxWagon = CloneWagon(middleWagon);
                    }
                }

                if (Mathf.Abs(dist - Mathf.Abs(stepSize)) < 0.0001f)
                {
                    break;
                }
            }

            if (safetyI >= 5000)
            {
                Debug.Log("safety");
            }


            wagon = CloneWagon(avgWagons(minWagon, maxWagon));
            wagon.position = Track.GetBezierPosition(wagon.t, wagon.previousElement, wagon.nextElement);

        }

        //Debug.Log(wagon.t+" : "+wagon.segment);
        newPos = wagon.position;

        localForward = Track.GetBezierForward(wagon.t, wagon.previousElement, wagon.nextElement);
        Vector3 localLeft = Track.GetBezierLeft(wagon.t, localForward, wagon.previousElement, wagon.nextElement, wagon.prevPreviousElement, wagon.nextNextElement);
        Vector3 localPosition = newPos;

        Vector3 leftWithNoRotation = Vector3.ProjectOnPlane(localForward, new Vector3(0f, 1f, 0f));
        if (Vector3.Magnitude(leftWithNoRotation) > 0.01f)
        {
            leftWithNoRotation = Vector3.Normalize(leftWithNoRotation);
            leftWithNoRotation = new Vector3(leftWithNoRotation.z, 0f, -leftWithNoRotation.x);
            float angle = Vector3.Angle(leftWithNoRotation, localLeft);
            if(localLeft.y < 0)
            {
                angle = -angle;
            }
            wagonPrefab.transform.localRotation = Quaternion.AngleAxis(angle, localForward) * Quaternion.LookRotation(localForward);
        }
        else
        {
            leftWithNoRotation = new Vector3(-1f, 0f, 0f);
            //Vector3 localForwardPerpendicular = Vector3.ProjectOnPlane(localForward, leftWithNoRotation);
            float angle = Vector3.Angle(leftWithNoRotation, localLeft);

            wagonPrefab.transform.localRotation = Quaternion.AngleAxis(Vector3.Angle(new Vector3(0f, 0f, 1f), localForward), localLeft) * Quaternion.AngleAxis(angle, localForward);
        }


        Vector3 wagonDisplacement = wagonPrefab.transform.localRotation * new Vector3(0f, -0.5f, 0f);
        wagon.position = newPos + wagonDisplacement;
        wagonPrefab.transform.localPosition = transform.TransformPoint(new Vector3(wagon.position.x, wagon.position.y, wagon.position.z));
    }
}