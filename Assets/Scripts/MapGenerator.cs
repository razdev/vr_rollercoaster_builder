﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public GameObject blocksRootParent;
    public GameObject tileBlock;
    public Vector3 mapDimensions;

    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow cube at the transform position
        
    }

    public void GenerateMap()
    {
        for(int i = 0; i < mapDimensions.x; i ++)
        {
            for (int j = 0; j < mapDimensions.y; j++)
            {
                for (int k = 0; k < mapDimensions.z; k++)
                {
                    Vector3 pos = new Vector3(-(mapDimensions.x / 2) + i, -(mapDimensions.y) + j, -(mapDimensions.z / 2) + k)*4;
                    Quaternion rot = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
                    GameObject plane = Instantiate(tileBlock, pos, rot, blocksRootParent.transform);
                    plane.transform.localEulerAngles = new Vector3(-90, 0, 0);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
