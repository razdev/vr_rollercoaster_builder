﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackElementSelector : MonoBehaviour {

	private TrackBuilder trackBuilder;
	private GameObject gameObject;
	public GameObject selectedGO;

	public void setTrackBuilderAndGameObject(TrackBuilder tb, GameObject obj) {
		trackBuilder = tb;
		gameObject = obj;
	}
	
	public void OnPointerOver(GameObject hit)
	{
		Instantiate(selectedGO);
		selectedGO.transform.localPosition = gameObject.transform.localPosition;
		Debug.Log("instantiante");
	}
		
    public void OnPointerExit(GameObject hit)
    {
        //if (hit.tag == "RollerCoaster")
            Destroy(selectedGO);
    }
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
