﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class CurveElement
{
    public Vector3 position;
    public Vector3 forward;
    public Vector3 left;
}

[Serializable]
public class Rail
{
    public Vector2 position;
    public float radius;
    public float fitTriangle;
    public float flattening;
    public int material;
    public int curveSegmentCount, pipeSegmentCount;
    public int startIndex;
}

public class Track : MonoBehaviour
{
    public List<CurveElement> curve;
    public List<Rail> rails;

    public static float GetAngleVec2D(Vector2 p_vector2)
    {
        if (p_vector2.x < 0)
        {
            return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
        }
        return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;
    }

    public static Vector3 GetBezierPosition(float t, CurveElement previousElement, CurveElement nextElement)
    {
        Vector3 p;

        Vector3 p0, p1, p2, p3;
        p0 = previousElement.position;
        p1 = previousElement.position + previousElement.forward;
        p2 = nextElement.position - nextElement.forward;
        p3 = nextElement.position;
        float omt = (1 - t);
        float omt2 = omt * omt;
        float t2 = t * t;
        p = p0 * omt2 * omt + 3 * p1 * t * omt2 + 3 * p2 * t2 * omt + p3 * t2 * t;

        return p;
    }
    //P : (-5.0, 0.0, -10.0)        (-11.0, 0.0, -9.0)
    public static Vector3 GetBezierForward(float t, CurveElement previousElement, CurveElement nextElement)
    {                                                       //F : (-4.0, 0.0 ,0.0)                (-3.5, 0.0, 1.5)
        float omt = 1f - t; //0.5
        float omt2 = omt * omt; //0.25
        float t2 = t * t;   //0.25

        //  (1.25, 0.0, 2.5) + (-9.0, 0.0, -10.0) * (-0.25) + (-7.5, 0.0, -10.5) * (0.25) + ( - 2.75, 0.0, -2.25)
        //
        // (1.25, 0.0, 2.5) + (2.25, 0.0, 2.5) + (-1.875, 0.0, -2.625) + (-2.75, 0.0, -2.25)
        //
        //   (3.5, 0.0, 5.0) + (-4.6.., 0.0, -4.9~)
        //
        //   (-1.1, 0.0, 0.1)
        Vector3 vec = previousElement.position * (-omt2) + (previousElement.position + previousElement.forward) * (3 * omt2 - 2 * omt) +
                (nextElement.position - nextElement.forward) * (-3 * t2 + 2 * t) + nextElement.position * (t2);
        float pn = Vector3.Magnitude(previousElement.forward);
        float nn = Vector3.Magnitude(nextElement.forward);
        float magn = (pn + nn) / 2.0f;
        return Vector3.Normalize(vec) * magn;
    }

    public static Vector3 GetBezierLeft(float t, Vector3 forward, CurveElement previousElement, CurveElement nextElement, CurveElement prevPreviousElement, CurveElement nextNextElement)
    {
        Vector3 leftBeginDirection = Vector3.Slerp(previousElement.left, nextElement.left, 0.5f) - Vector3.Slerp(prevPreviousElement.left, previousElement.left, 0.5f);
        leftBeginDirection = Vector3.Normalize(leftBeginDirection) * 0.03f;
        Vector3 leftEndDirection = Vector3.Slerp(nextElement.left, nextNextElement.left, 0.5f) - Vector3.Slerp(previousElement.left, nextElement.left, 0.5f);
        leftEndDirection = Vector3.Normalize(leftEndDirection) * 0.03f;
        Vector3 left00 = Vector3.Slerp(previousElement.left, previousElement.left + leftBeginDirection, t);
        Vector3 left01 = Vector3.Slerp(previousElement.left + leftBeginDirection, nextElement.left - leftEndDirection, t);
        Vector3 left02 = Vector3.Slerp(nextElement.left - leftEndDirection, nextElement.left, t);
        Vector3 left10 = Vector3.Slerp(left00, left01, t);
        Vector3 left11 = Vector3.Slerp(left01, left02, t);
        return Vector3.Normalize(Vector3.ProjectOnPlane(Vector3.Slerp(left10, left11, t), forward));
    }

    private Vector3 GetPointOnRail(float u, float v, CurveElement previousElement, CurveElement nextElement, CurveElement prevPreviousElement, CurveElement nextNextElement, Rail rail)
    {
        Vector3 p = GetBezierPosition(v, previousElement, nextElement);

        Vector3 forward = GetBezierForward(v, previousElement, nextElement);

        Vector3 left = GetBezierLeft(v, forward, previousElement, nextElement, prevPreviousElement, nextNextElement);

        float railTheta = GetAngleVec2D(rail.position);

        Vector3 railDisplacement; // displacement due to the position of the rail
        railDisplacement = Quaternion.AngleAxis(GetAngleVec2D(rail.position) + 90, forward) * left;
        railDisplacement *= rail.position.magnitude;

        p = p + railDisplacement;

        Vector3 cylinderDisplacement; // displacement due to the rotation
        cylinderDisplacement = Quaternion.AngleAxis(360f * u, forward) * left;
        //cylinderDisplacement *= - Mathf.Sin(u*2f*3.14159265f) * rail.flattening;
        Vector3 up = Vector3.Cross(forward, left);
        //cylinderDisplacement += left * Mathf.Sin(u * 2f * 3.14159265f) * rail.flattening;
        cylinderDisplacement += left * (-Mathf.Sin(2f * 3.14159265f * u)) * rail.fitTriangle;
        cylinderDisplacement *= rail.radius;

        return p + cylinderDisplacement;
    }

    private void OnDrawGizmos()
    {
        normalizeLeftCurve();


        for (int r = 0; r < rails.Count; r++)
        {
            Rail rail = rails[r];
            float uStep = 1f / rail.curveSegmentCount;
            float vStep = 1f / rail.pipeSegmentCount;
            for (int c = 0; c < curve.Count; c++)
            {
                CurveElement previousElement = curve[c];
                CurveElement nextElement = curve[(c + 1) % curve.Count];
                CurveElement prevPreviousElement = curve[(c + curve.Count - 1) % curve.Count];
                CurveElement nextNextElement = curve[(c + 2) % curve.Count];
                for (int u = 0; u < rail.curveSegmentCount; u++)
                {
                    for (int v = 0; v < rail.pipeSegmentCount; v++)
                    {
                        Vector3 point = GetPointOnRail((float)u * uStep, (float)v * vStep, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
                        point = transform.TransformPoint(point);
                        Gizmos.color = new Color(
                            1f,
                            (float)v * uStep,
                            (float)u * vStep);
                        Gizmos.DrawSphere(point, 0.1f);
                    }
                }
            }
        }
    }

    private Mesh mesh;
    private Vector3[] vertices;
    private int[] triangles;
    private Material[] materials;

    private void Awake()
    {
        materials = GetComponent<Renderer>().materials;

        UpdateAll();
    }

    public void UpdatePositionsAround(int position)
    {
        normalizeLeftCurve();
        SetVerticesAround(position);
        mesh.RecalculateNormals();
    }

    public void UpdateAll()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Track";

        normalizeLeftCurve();
        SetVertices();
        MySetTriangles();
        mesh.RecalculateNormals();
    }

    private void normalizeLeftCurve()
    {
        for (int c = 0; c < curve.Count; c++)
        {
            curve[c].left = Vector3.Normalize(Vector3.ProjectOnPlane(curve[c].left, curve[c].forward));
        }
    }

    private void SetVertices()
    {
        vertices = new Vector3[getQuadNum() * 4];
        int i = 0;
        for (int r = 0; r < rails.Count; r++)
        {
            Rail rail = rails[r];
            rails[r].startIndex = i;
            float uStep = 1f / rail.curveSegmentCount;
            int iDelta = rail.pipeSegmentCount * 4;
            for (int c = 0; c < curve.Count; c++)
            {
                CurveElement previousElement = curve[c];
                CurveElement nextElement = curve[(c + 1) % curve.Count];
                CurveElement prevPreviousElement = curve[(c + curve.Count - 1) % curve.Count];
                CurveElement nextNextElement = curve[(c + 2) % curve.Count];
                CreateFirstQuadRing((float)uStep, i, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
                i = i + iDelta;
                for (int u = 2; u <= rail.curveSegmentCount; u++, i += iDelta)
                {
                    CreateQuadRing((float)u * uStep, i, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
                }
            }
        }
        mesh.vertices = vertices;
    }

    private void SetVerticesAround(int position)
    {
        int i = 0;
        for (int r = 0; r < rails.Count; r++)
        {
            Rail rail = rails[r];
            rails[r].startIndex = i;
            float uStep = 1f / rail.curveSegmentCount;
            int iDelta = rail.pipeSegmentCount * 4;
            for (int c = 0; c < curve.Count; c++)
            {
                if (c != position && c != (position + curve.Count - 1) % curve.Count && c != (position + 1) % curve.Count && c != (position + 2) % curve.Count)
                {
                    i = i + iDelta * rail.curveSegmentCount;
                    continue;
                }

                CurveElement previousElement = curve[c];
                CurveElement nextElement = curve[(c + 1) % curve.Count];
                CurveElement prevPreviousElement = curve[(c + curve.Count - 1) % curve.Count];
                CurveElement nextNextElement = curve[(c + 2) % curve.Count];
                CreateFirstQuadRing((float)uStep, i, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
                i = i + iDelta;
                for (int u = 2; u <= rail.curveSegmentCount; u++, i += iDelta)
                {
                    CreateQuadRing((float)u * uStep, i, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
                }
            }
        }
        mesh.vertices = vertices;
    }

    private void CreateFirstQuadRing(float u, int i, CurveElement previousElement, CurveElement nextElement, CurveElement prevPreviousElement, CurveElement nextNextElement, Rail rail)
    {
        float vStep = 1f / rail.pipeSegmentCount;

        Vector3 vertexA = GetPointOnRail(0f, 0f, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
        Vector3 vertexB = GetPointOnRail((float)u, 0f, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
        for (int v = 1; v <= rail.pipeSegmentCount; v++, i += 4)
        {
            vertices[i] = vertexA;
            vertices[i + 1] = vertexA = GetPointOnRail(0f, (float)v / rail.pipeSegmentCount, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
            vertices[i + 2] = vertexB;
            vertices[i + 3] = vertexB = GetPointOnRail((float)u, (float)v / rail.pipeSegmentCount, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
        }
    }

    private void CreateQuadRing(float u, int i, CurveElement previousElement, CurveElement nextElement, CurveElement prevPreviousElement, CurveElement nextNextElement, Rail rail)
    {
        float vStep = 1f / rail.pipeSegmentCount;
        int ringOffset = rail.pipeSegmentCount * 4;

        Vector3 vertex = GetPointOnRail((float)u, 0f, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
        for (int v = 1; v <= rail.pipeSegmentCount; v++, i += 4)
        {
            vertices[i] = vertices[i - ringOffset + 2];
            vertices[i + 1] = vertices[i - ringOffset + 3];
            vertices[i + 2] = vertex;
            vertices[i + 3] = vertex = GetPointOnRail((float)u, (float)v / rail.pipeSegmentCount, previousElement, nextElement, prevPreviousElement, nextNextElement, rail);
        }
    }

    private int getQuadNum()
    {
        int sum = 0;
        for (int r = 0; r < rails.Count; r++)
        {
            sum += rails[r].pipeSegmentCount * rails[r].curveSegmentCount * curve.Count;
        }
        return sum;
    }

    private int getQuadNumHavingMaterial(int mat)
    {
        int sum = 0;
        for (int r = 0; r < rails.Count; r++)
        {
            if (rails[r].material == mat)
            {
                sum += rails[r].pipeSegmentCount * rails[r].curveSegmentCount * curve.Count;
            }
        }
        return sum;
    }

    private void MySetTriangles()
    {
        mesh.subMeshCount = materials.Length;
        for (int m = 0; m < materials.Length; ++m)
        {
            triangles = new int[getQuadNumHavingMaterial(m) * 6];
            int i = 0;
            int t = 0;
            int ir;
            for (int r = 0; r < rails.Count; r++)
            {
                if (rails[r].material != m)
                {
                    continue;
                }

                int quadNumInRail = rails[r].pipeSegmentCount * rails[r].curveSegmentCount * curve.Count;
                i = rails[r].startIndex;
                for (ir = 0; t < triangles.Length && ir < quadNumInRail; t += 6)
                {
                    triangles[t] = i;
                    triangles[t + 1] = triangles[t + 4] = i + 2;
                    triangles[t + 2] = triangles[t + 3] = i + 1;
                    triangles[t + 5] = i + 3;
                    i += 4;
                }
            }
            mesh.SetTriangles(triangles, m);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}