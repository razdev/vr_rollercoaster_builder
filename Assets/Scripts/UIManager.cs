﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject UIParent;
    [Header("Main menu")]
    public GameObject buildingUIBackground;
    public RawImage option1;
    public RawImage option2;
    public RawImage option3;
    public RawImage option4;
    public bool onBuildUI = false;

    [Header("Coaster editor")]
    public GameObject editUIBackground;
    public RawImage editButton;
    public RawImage addSupprButton;
    public RawImage freeCamButton;
    public RawImage topDownButton;
    public RawImage finishButton;
    public RawImage editModeRectangle;
    public RawImage camModeRectangle;
    public bool onEditUI = true;

    public TextMeshProUGUI playerInstructions;
    public OculusRemote oculusRemoteScript;
    public PlayerMovement playerMoveScript;
    public CoasterEditor coasterEditor;


    public void showbuildingUI(bool active)
    {
        buildingUIBackground.SetActive(active);
    }

    public void enabledPlayerInstructionsText(bool enable)
    {
        playerInstructions.enabled = enable;
    }

    public void setPlayerInstructionText(string text)
    {
        playerInstructions.text = text;
    }

    public void showEditingUI(bool active)
    {
        editUIBackground.SetActive(active);
    }

    public void changeUIMode(bool flyMode)
    {
        if(flyMode)
        {
            buildingUIBackground.GetComponent<BoxCollider>().size = new Vector3(1600, 450, 0.001f);
            buildingUIBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(1600, 450);
            option1.gameObject.SetActive(true);
            option2.gameObject.SetActive(true);
            option3.GetComponent<RectTransform>().anchoredPosition = new Vector2(200, 25);
            option4.GetComponent<RectTransform>().anchoredPosition = new Vector2(600, 25);
        }
        else
        {
            buildingUIBackground.GetComponent<BoxCollider>().size = new Vector3(800, 450, 0.001f);
            buildingUIBackground.GetComponent<RectTransform>().sizeDelta = new Vector2(800, 450);
            option1.gameObject.SetActive(false);
            option2.gameObject.SetActive(false);
            option3.GetComponent<RectTransform>().anchoredPosition = new Vector2(-200, 25);
            option4.GetComponent<RectTransform>().anchoredPosition = new Vector2(200, 25);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {

        if (oculusRemoteScript.backPressed && !playerMoveScript.isEditing && !coasterEditor.isPosing)// OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            onBuildUI = !onBuildUI;
            showbuildingUI(onBuildUI);
        }

        if (oculusRemoteScript.backPressed && playerMoveScript.isEditing)// OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            onEditUI = !onEditUI;
            coasterEditor.onMenu = !coasterEditor.onMenu;
            showEditingUI(onEditUI);
        }
    }
}
