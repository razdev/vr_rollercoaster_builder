﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoasterEditor : MonoBehaviour
{

    public int gunDamage = 1;                                            // Set the number of hitpoints that this gun will take away from shot objects with a health script
    public float fireRate = 0.25f;                                        // Number in seconds which controls how often the player can fire
    public float weaponRange = 50f;                                        // Distance in Unity units over which the player can fire
    public float hitForce = 100f;                                        // Amount of force which will be added to objects with a rigidbody shot by the player
    public Transform gunEnd;                                            // Holds a reference to the gun end object, marking the muzzle location of the gun

    public Camera fpsCam;                                                // Holds a reference to the first person camera
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);    // WaitForSeconds object used by our ShotEffect coroutine, determines time laser line will remain visible
    private AudioSource gunAudio;                                        // Reference to the audio source which will play our shooting sound effect
    private LineRenderer laserLine;                                        // Reference to the LineRenderer component which will display our laserline
    private float nextFire;                                                // Float to store the time the player will be allowed to fire again, after firing

    public GameObject camTracker;

    public GameObject option1;
    public GameObject option2;
    public GameObject option3;
    public GameObject option4;
    public CoasterAttraction attractionScript;

    public UIManager uiScript;
    public OculusRemote oculusRemoteScript;
    public PlayerMovement playerMoveScript;
    public bool isPosing = false;

    [Header("Attraction prefabs")]
    public GameObject coasterPrefab;
    public GameObject ferrisWheelPrefab;
    public GameObject currentAttractionInstance;

    [Header("Blocks wireframe materials")]
    public GameObject selectedBlock = null;
    public Material notSelected;
    public Material selected;
    public GameObject selectedAttraction = null;
    public GameObject OVRCam;

    [Header("Editor")]
    public GameObject addSphere;
    public GameObject supprSphere;
    public GameObject hightlightSpheres;
    public int currentHighlightIndex;
    public GameObject editSphereTD;
    public GameObject editSphereFree;
    public GameObject editArrow;
    public bool dragingSphere = false;
    public bool dragingArrow = false;
    public bool onMenu = true;
    public GameObject draggedSphere = null;
    public GameObject draggedArrow = null;
    public GameObject draggedTorus = null;
    public bool dragingTorus = false;
    public Vector3 referencePosition;
    public Vector3 referenceForward;

    void Start()
    {
        // Get and store a reference to our LineRenderer component
        laserLine = GetComponent<LineRenderer>();

    }

    

    void LateUpdate()
    {
        if (dragingSphere && !oculusRemoteScript.triggerIsDown)
        {
            dragingSphere = false;
            draggedSphere = null;
        }
        if (dragingArrow && !oculusRemoteScript.triggerIsDown)
        {
            dragingArrow = false;
            draggedArrow = null;
        }
        if (dragingTorus && !oculusRemoteScript.triggerIsDown)
        {
           

            dragingTorus = false;
            draggedTorus = null;
        }

        GameObject previousBlock = selectedBlock;

        // Create a vector at the center of our camera's viewport
        Vector3 rayOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));

        // Declare a raycast hit to store information about what our raycast has hit
        RaycastHit hit;

        // Set the start position for our visual effect for our laser to the position of gunEnd
        laserLine.SetPosition(0, gunEnd.position);

        // Check if our raycast has hit anything
        if (!playerMoveScript.inAttraction && Physics.Raycast(gunEnd.position, oculusRemoteScript.transform.forward, out hit, weaponRange))
        {
            // Set the end position for our laser line 
            laserLine.SetPosition(1, hit.point);

            //Debug.Log(hit.point + " " + hit.transform.gameObject);
            //laserLine.startWidth = Vector3.Distance(gunEnd.position, hit.point) / 100;
            laserLine.endWidth = Vector3.Distance(gunEnd.position, hit.point) / 100;


            if (!dragingTorus && !dragingSphere && !dragingArrow)
            {
                List<GameObject> a = new List<GameObject>(GameObject.FindGameObjectsWithTag("HorizontalTorus"));
                List<GameObject> b = new List<GameObject>(GameObject.FindGameObjectsWithTag("VerticalTorus"));
                List<GameObject> c = new List<GameObject>(GameObject.FindGameObjectsWithTag("EditNode"));
                List<GameObject> d = new List<GameObject>(GameObject.FindGameObjectsWithTag("EditArrow"));
                a.AddRange(b);
                a.AddRange(c);
                a.AddRange(d);

                GameObject[] un = a.ToArray();

                GameObject drag = oculusRemoteScript.getSelectedObject(un, 10f);
                if(drag != null)
                {
                    draggedArrow = null;
                    draggedSphere = null;
                    draggedTorus = null;
                    if (drag.gameObject.tag == "HorizontalTorus" || drag.gameObject.tag == "VerticalTorus")
                        draggedTorus = drag;
                    if (drag.gameObject.tag == "EditNode")
                        draggedSphere = drag;
                    if (drag.gameObject.tag == "EditArrow")
                        draggedArrow = drag;
                }
            }


            // Check if the object we hit has a rigidbody attached
            if (hit.rigidbody != null)
            {
                // Add force to the rigidbody we hit, in the direction from which it was hit
                hit.rigidbody.AddForce(-hit.normal * hitForce);
            }

            if (hit.transform.gameObject.tag == "Delete" && oculusRemoteScript.triggerPressed)
            {
                if(hit.transform.root.gameObject.GetComponent<CoasterAttraction>() != null)
                    Destroy(hit.transform.root.gameObject.GetComponent<CoasterAttraction>().wagonPrefab.gameObject);
                Destroy(hit.transform.root.gameObject);

                return;
            }


            if (hit.transform.gameObject == option1 && oculusRemoteScript.triggerPressed)
            {
                //attractionScript.climbIn();
                uiScript.onBuildUI = !uiScript.onBuildUI;
                uiScript.showbuildingUI(uiScript.onBuildUI);

                uiScript.setPlayerInstructionText("Viser un bloc et pressez <sprite=6> pour placer la base d'une montagne russe");
                uiScript.enabledPlayerInstructionsText(true);

                isPosing = true;
                currentAttractionInstance = Instantiate(coasterPrefab).gameObject;
            }

            if (hit.transform.gameObject == option2 && oculusRemoteScript.triggerPressed)
            {
                //attractionScript.climbIn();
                uiScript.onBuildUI = !uiScript.onBuildUI;
                uiScript.showbuildingUI(uiScript.onBuildUI);

                uiScript.setPlayerInstructionText("Viser un bloc et pressez <sprite=6> pour placer une grande roue");
                uiScript.enabledPlayerInstructionsText(true);

                isPosing = true;
                currentAttractionInstance = Instantiate(ferrisWheelPrefab).gameObject;
            }

            if (hit.transform.gameObject == option3 && oculusRemoteScript.triggerPressed)
            {
                playerMoveScript.SwitchMode();
            }

            if (hit.transform.gameObject == option4 && oculusRemoteScript.triggerPressed)
            {
                Application.Quit();
            }

            if (hit.transform.gameObject.tag == "Edit" && oculusRemoteScript.triggerPressed)
            {
                uiScript.onBuildUI = false;
                uiScript.showbuildingUI(uiScript.onBuildUI);
                uiScript.onEditUI = true;
                playerMoveScript.isEditing = true;
                DisableCoastersUI();
                InstantiateEditObjects(playerMoveScript.camMode == 0);

                playerMoveScript.isEditing = true;
                uiScript.showEditingUI(true);
            }

            if (isPosing && hit.transform.gameObject.tag == "GrassBlock" && oculusRemoteScript.triggerPressed)
            {
                Debug.Log("ah");
                isPosing = false;
                uiScript.enabledPlayerInstructionsText(false);
                oculusRemoteScript.triggerPressed = false;
                switch (currentAttractionInstance.transform.GetChild(0).GetComponent<AttractionType>().attractionType)
                {
                    /*case AttrType.roller_coaster:
                        BoxCollider box1 = currentAttractionInstance.gameObject.AddComponent<BoxCollider>();
                        box1.isTrigger = true;
                        break;*/

                    case AttrType.ferris_wheel:
                        BoxCollider box2 = currentAttractionInstance.transform.GetChild(2).gameObject.AddComponent<BoxCollider>();
                        box2.isTrigger = true;
                        box2.size = new Vector3(box2.size.x + 2, box2.size.y + 2, box2.size.z + 2);
                        break;
                }

                Material[] mat = hit.collider.gameObject.GetComponent<MeshRenderer>().materials;
                mat[1] = notSelected;
                hit.collider.gameObject.GetComponent<MeshRenderer>().materials = mat;
            }

            if (isPosing && hit.transform.gameObject.tag == "GrassBlock")
            {
                currentAttractionInstance.transform.position = new Vector3(hit.transform.position.x, currentAttractionInstance.transform.position.y, hit.transform.position.z);
                Material[] mat = hit.collider.gameObject.GetComponent<MeshRenderer>().materials;
                mat[1] = selected;
                hit.collider.gameObject.GetComponent<MeshRenderer>().materials = mat;
                selectedBlock = hit.collider.gameObject;
            }

            if (previousBlock != null && previousBlock != selectedBlock)
            {
                Material[] mat = previousBlock.GetComponent<MeshRenderer>().materials;
                mat[1] = notSelected;
                previousBlock.GetComponent<MeshRenderer>().materials = mat;
            }


            if (hit.transform.gameObject == uiScript.editButton.gameObject && oculusRemoteScript.triggerPressed)
            {
                uiScript.editModeRectangle.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(
                    uiScript.editButton.GetComponent<RectTransform>().anchoredPosition.x,
                    uiScript.editButton.GetComponent<RectTransform>().anchoredPosition.y
                );

                playerMoveScript.editMode = 0;

                DeleteAddSupprSpheres();
                ResetAddSupprSpheresScale();
                DeleteEditSpheres();
                InstantiateEditObjects(playerMoveScript.camMode == 0);
            }
            if (hit.transform.gameObject == uiScript.addSupprButton.gameObject && oculusRemoteScript.triggerPressed)
            {
                uiScript.editModeRectangle.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(
                    uiScript.addSupprButton.GetComponent<RectTransform>().anchoredPosition.x,
                    uiScript.addSupprButton.GetComponent<RectTransform>().anchoredPosition.y
                );

                playerMoveScript.editMode = 1;

                DeleteAddSupprSpheres();
                ResetAddSupprSpheresScale();
                DeleteEditSpheres();
                InstantiateAddSupprObjects();
            }
            if (hit.transform.gameObject == uiScript.freeCamButton.gameObject && oculusRemoteScript.triggerPressed)
            {
                uiScript.camModeRectangle.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(
                    uiScript.freeCamButton.GetComponent<RectTransform>().anchoredPosition.x,
                    uiScript.freeCamButton.GetComponent<RectTransform>().anchoredPosition.y
                );

                playerMoveScript.camMode = 0;

                if(playerMoveScript.editMode == 0)
                {
                    DeleteEditSpheres();
                    InstantiateEditObjects(playerMoveScript.camMode == 0);
                }

                StartCoroutine(smoothCamFlip());
            }
            if (hit.transform.gameObject == uiScript.topDownButton.gameObject && oculusRemoteScript.triggerPressed)
            {
                uiScript.camModeRectangle.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(
                    uiScript.topDownButton.GetComponent<RectTransform>().anchoredPosition.x,
                    uiScript.topDownButton.GetComponent<RectTransform>().anchoredPosition.y
                );

                playerMoveScript.camMode = 1;

                if (playerMoveScript.editMode == 0)
                {
                    DeleteEditSpheres();
                    InstantiateEditObjects(playerMoveScript.camMode == 0);
                }

                StartCoroutine(smoothCamFlip());
            }
            if (hit.transform.gameObject == uiScript.finishButton.gameObject && oculusRemoteScript.triggerPressed)
            {
                uiScript.camModeRectangle.GetComponent<RectTransform>().anchoredPosition =
                new Vector2(
                    uiScript.freeCamButton.GetComponent<RectTransform>().anchoredPosition.x,
                    uiScript.freeCamButton.GetComponent<RectTransform>().anchoredPosition.y
                );

                if (playerMoveScript.editMode == 0)
                    DeleteEditSpheres();
                else
                {
                    DeleteAddSupprSpheres();
                    ResetAddSupprSpheresScale();
                }
                    
                uiScript.onEditUI = false;
                playerMoveScript.isEditing = false;
                uiScript.showEditingUI(false);


                playerMoveScript.camMode = 0;

                StartCoroutine(smoothCamFlip());
            }

            GameObject sphere = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("AddSupprNode"));
			
			
			
			//Debug.Log("selected torus : "+torus.name);

            if (playerMoveScript.editMode == 1 && sphere != null && !onMenu && oculusRemoteScript.triggerPressed)
            {
                ResetAddSupprSpheresScale();
                DeleteAddSupprSpheres();
                

                int type = sphere.transform.gameObject.GetComponent<NodeType>().type;
                int index = sphere.transform.gameObject.GetComponent<NodeType>().index;

                if (type == 0)
                    currentAttractionInstance.GetComponent<CoasterAttraction>().addElement(index);
                else if (type == 1)
                    currentAttractionInstance.GetComponent<CoasterAttraction>().deleteElement(index);

                InstantiateAddSupprObjects();
            }

            //if ((!dragingSphere || draggedSphere == null) && draggedArrow == null && draggedTorus == null)
            //    draggedSphere = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("EditNode"));
            if (dragingSphere || (playerMoveScript.editMode == 0 && draggedSphere != null && !onMenu && oculusRemoteScript.triggerPressed))
            {
                dragingSphere = true;
                int index = draggedSphere.transform.gameObject.GetComponent<NodeType>().index;

                GameObject[] arrows = GameObject.FindGameObjectsWithTag("EditArrow");
                GameObject respectiveArrow = null;
                foreach (GameObject arrow in arrows)
                {
                    if (arrow.GetComponent<NodeType>().index == index)
                        respectiveArrow = arrow;
                }

                SetDragPoint(draggedSphere, respectiveArrow);

                currentAttractionInstance.GetComponent<CoasterAttraction>().moveElement(index, draggedSphere.transform.localPosition);

            }


            


            //if ((!dragingArrow || draggedArrow == null) && draggedSphere == null && draggedTorus == null)
            //    draggedArrow = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("EditArrow"));
            if (dragingArrow || (playerMoveScript.editMode == 0 && draggedArrow != null && !onMenu && oculusRemoteScript.triggerPressed))
            {
                dragingArrow = true;
                int index = draggedArrow.transform.gameObject.GetComponent<NodeType>().index;

                GameObject[] spheres = GameObject.FindGameObjectsWithTag("EditNode");
                GameObject respectiveSphere = null;
                foreach (GameObject spheree in spheres)
                {
                    if (spheree.GetComponent<NodeType>().index == index)
                        respectiveSphere = spheree;
                }

                SetDragPointArrow(draggedArrow, respectiveSphere);

                currentAttractionInstance.GetComponent<CoasterAttraction>().moveElement(index, draggedArrow.transform.localPosition - new Vector3(0, 4, 0));

            }

                
            if (dragingTorus || (playerMoveScript.editMode == 0 && draggedTorus != null && !onMenu && oculusRemoteScript.triggerPressed))
            {
                int index = draggedTorus.transform.parent.gameObject.GetComponent<NodeType>().index;
                if (!dragingTorus)
                {
                    referencePosition = GetHittedPoint(draggedTorus);
                    referenceForward = currentAttractionInstance.GetComponent<Track>().curve[index].forward;
                }

                dragingTorus = true;

                float angle = GetDragTorusRotation(draggedTorus, referencePosition);

                Debug.Log(angle);

                currentAttractionInstance.GetComponent<CoasterAttraction>().changeElementDirection(index, Quaternion.AngleAxis(angle, new Vector3(0, 1.0f, 0)) * referenceForward);
            }
        }
        else
        {
            // If we did not hit anything, set the end of the line to a position directly in front of the camera at the distance of weaponRange
            laserLine.SetPosition(1, gunEnd.position + (oculusRemoteScript.transform.forward * weaponRange));

            if ((!dragingSphere || draggedSphere == null) && draggedArrow == null)
                draggedSphere = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("EditNode"));
            if (dragingSphere || (playerMoveScript.editMode == 0 && draggedSphere != null && !onMenu && oculusRemoteScript.triggerPressed))
            {
                dragingSphere = true;
                int index = draggedSphere.transform.gameObject.GetComponent<NodeType>().index;

                GameObject[] arrows = GameObject.FindGameObjectsWithTag("EditArrow");
                GameObject respectiveArrow = null;
                foreach (GameObject arrow in arrows)
                {
                    if (arrow.GetComponent<NodeType>().index == index)
                        respectiveArrow = arrow;
                }

                SetDragPoint(draggedSphere, respectiveArrow);

                currentAttractionInstance.GetComponent<CoasterAttraction>().moveElement(index, draggedSphere.transform.localPosition);

            }

            if ((!dragingArrow || draggedArrow == null) && draggedSphere == null)
                draggedArrow = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("EditArrow"));
            if (dragingArrow || (playerMoveScript.editMode == 0 && draggedArrow != null && !onMenu && oculusRemoteScript.triggerPressed))
            {
                dragingArrow = true;
                int index = draggedArrow.transform.gameObject.GetComponent<NodeType>().index;

                GameObject[] spheres = GameObject.FindGameObjectsWithTag("EditNode");
                GameObject respectiveSphere = null;
                foreach (GameObject spheree in spheres)
                {
                    if (spheree.GetComponent<NodeType>().index == index)
                        respectiveSphere = spheree;
                }

                SetDragPointArrow(draggedArrow, respectiveSphere);

                currentAttractionInstance.GetComponent<CoasterAttraction>().moveElement(index, draggedArrow.transform.localPosition - new Vector3(0, 4, 0));

            }
        }

        if(!isPosing && playerMoveScript.flyMode && !playerMoveScript.isEditing)
        {
            selectedAttraction = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("LaserCollider"));
            currentAttractionInstance = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("LaserCollider"));
            if (currentAttractionInstance != null)
                currentAttractionInstance = currentAttractionInstance.transform.root.gameObject;
            DisableCoastersUI();

            if (selectedAttraction != null)
            {
                Transform background = selectedAttraction.transform.GetChild(0).transform.GetChild(0);
                background.GetComponent<RawImage>().enabled = true;
                background.GetComponent<BoxCollider>().enabled = true;
                background.transform.GetChild(0).GetComponent<RawImage>().enabled = true;
                background.transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
                background.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().enabled = true;

                if (selectedAttraction.GetComponent<AttractionType>().attractionType == AttrType.roller_coaster)
                {
                    background.transform.GetChild(1).GetComponent<RawImage>().enabled = true;
                    background.transform.GetChild(1).GetComponent<BoxCollider>().enabled = true;
                    background.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().enabled = true;
                }
            }
        }

        if (playerMoveScript.isEditing && playerMoveScript.editMode == 1)
        {
            GameObject selectedNode = oculusRemoteScript.getSelectedObject(GameObject.FindGameObjectsWithTag("AddSupprNode"));
            ResetAddSupprSpheresScale();
            if (selectedNode != null && !onMenu)
            {
                hightlightSpheres.SetActive(true);
                hightlightSpheres.transform.SetParent(currentAttractionInstance.transform);
                hightlightSpheres.transform.localPosition = new Vector3(0, 0, 0);

                selectedNode.transform.localScale = new Vector3(170f, 170f, 170f);

                int type = selectedNode.GetComponent<NodeType>().type;
                int index = selectedNode.GetComponent<NodeType>().index;

                for (int i = 1; i <= hightlightSpheres.transform.childCount; i++)
                {
                    GameObject sphere = hightlightSpheres.transform.GetChild(i - 1).gameObject;
                    if (type == 0)
                    {
                        sphere.transform.localPosition = Track.GetBezierPosition(0.5f + 0.025f * i, currentAttractionInstance.GetComponent<Track>().curve[(index - 1) % currentAttractionInstance.GetComponent<Track>().curve.Count], currentAttractionInstance.GetComponent<Track>().curve[index % currentAttractionInstance.GetComponent<Track>().curve.Count]);
                    }
                    else if (type == 1)
                    {
                        if (i < 10)
                            sphere.transform.localPosition = Track.GetBezierPosition(0.05f * i, currentAttractionInstance.GetComponent<Track>().curve[index % currentAttractionInstance.GetComponent<Track>().curve.Count], currentAttractionInstance.GetComponent<Track>().curve[(index + 1) % currentAttractionInstance.GetComponent<Track>().curve.Count]);
                        else
                            sphere.transform.localPosition = Track.GetBezierPosition(0.05f + 0.05f * i, currentAttractionInstance.GetComponent<Track>().curve[(index - 1) % currentAttractionInstance.GetComponent<Track>().curve.Count], currentAttractionInstance.GetComponent<Track>().curve[index % currentAttractionInstance.GetComponent<Track>().curve.Count]);
                    }

                }


            }
        }

        if (playerMoveScript.isEditing && playerMoveScript.editMode == 0)
        {
            ResetEditSpheresScale();
            if (draggedSphere != null && !onMenu)
            {
                draggedSphere.transform.localScale = new Vector3(170f, 170f, 170f);
            }
            if (draggedArrow != null && !onMenu)
            {
                draggedArrow.transform.localScale = new Vector3(1.7f, 1.7f, 1.7f);
            }
            if (draggedTorus != null && !onMenu)
            {
                draggedTorus.transform.localScale = new Vector3(1.7f, 1.7f, 1.7f);
            }
        }
    }

    public void ResetAddSupprSpheresScale()
    {
        foreach (GameObject sphere in GameObject.FindGameObjectsWithTag("AddSupprNode"))
        {
            sphere.transform.localScale = new Vector3(100f, 100f, 100f);
        }
        hightlightSpheres.transform.SetParent(null);
        hightlightSpheres.SetActive(false);
    }

    public void ResetEditSpheresScale()
    {
        foreach (GameObject sphere in GameObject.FindGameObjectsWithTag("EditNode"))
        {
            sphere.transform.localScale = new Vector3(100f, 100f, 100f);
        }
        foreach (GameObject arrow in GameObject.FindGameObjectsWithTag("EditArrow"))
        {
            arrow.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        foreach (GameObject torus in GameObject.FindGameObjectsWithTag("HorizontalTorus"))
        {
            torus.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        foreach (GameObject torus in GameObject.FindGameObjectsWithTag("VerticalTorus"))
        {
            torus.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    public void DeleteAddSupprSpheres()
    {
        GameObject[] spheres = GameObject.FindGameObjectsWithTag("AddSupprNode");
        for (int i = 0; i < spheres.Length; i++)
        {
            Destroy(spheres[i].gameObject);
        }
    }

    public void DeleteEditSpheres()
    {
        GameObject[] spheres = GameObject.FindGameObjectsWithTag("EditNode");
        for (int i = 0; i < spheres.Length; i++)
        {
            Destroy(spheres[i].gameObject);
        }

        GameObject[] arrow = GameObject.FindGameObjectsWithTag("EditArrow");
        for (int i = 0; i < arrow.Length; i++)
        {
            Destroy(arrow[i].gameObject);
        }

        GameObject[] torus = GameObject.FindGameObjectsWithTag("HorizontalTorus");
        for (int i = 0; i < torus.Length; i++)
        {
            Destroy(torus[i].gameObject);
        }

        GameObject[] torus2 = GameObject.FindGameObjectsWithTag("VerticalTorus");
        for (int i = 0; i < torus2.Length; i++)
        {
            Destroy(torus2[i].gameObject);
        }
    }

    public void InstantiateEditObjects(bool freeMode)
    {
        for (int i = 5; i < currentAttractionInstance.GetComponent<Track>().curve.Count; i++)
        {
            GameObject sphereEdit;
            if (freeMode)
                sphereEdit = Instantiate(editSphereFree, currentAttractionInstance.transform);
            else
                sphereEdit = Instantiate(editSphereTD, currentAttractionInstance.transform);
            sphereEdit.transform.localPosition = currentAttractionInstance.GetComponent<Track>().curve[i].position;
            sphereEdit.GetComponent<NodeType>().index = i;

            if (freeMode)
            {
                GameObject upDownArrow;
                upDownArrow = Instantiate(editArrow, currentAttractionInstance.transform);
                upDownArrow.transform.localPosition = currentAttractionInstance.GetComponent<Track>().curve[i].position + new Vector3(0, 4, 0);
                upDownArrow.GetComponent<NodeType>().index = i;
            }
        }
    }

    public void InstantiateAddSupprObjects()
    {
        for (int i = 5; i < currentAttractionInstance.GetComponent<Track>().curve.Count; i++)
        {
            GameObject sphereSuppr = Instantiate(supprSphere, currentAttractionInstance.transform);
            sphereSuppr.transform.localPosition = currentAttractionInstance.GetComponent<Track>().curve[i].position;
            sphereSuppr.GetComponent<NodeType>().index = i;


        }
        for (int i = 5; i <= currentAttractionInstance.GetComponent<Track>().curve.Count; i++)
        {
            GameObject sphereAdd = Instantiate(addSphere, currentAttractionInstance.transform);
            sphereAdd.transform.localPosition = Track.GetBezierPosition(0.5f, currentAttractionInstance.GetComponent<Track>().curve[i - 1], currentAttractionInstance.GetComponent<Track>().curve[i % currentAttractionInstance.GetComponent<Track>().curve.Count]);
            sphereAdd.GetComponent<NodeType>().index = i;
        }
    }

    public IEnumerator smoothCamFlip()
    {
        float t = 0f;

        float targetRotx = 90;
        if (playerMoveScript.camMode == 0)
            targetRotx = 20;

        float initRotx = OVRCam.GetComponent<Transform>().localEulerAngles.x;

        while (t < 1.0f)
        {
            t += Time.deltaTime;
            OVRCam.GetComponent<Transform>().localEulerAngles =
                new Vector3(
                    Mathf.Lerp(initRotx, targetRotx, Mathf.SmoothStep(0.0f, 1.0f, t)),
                    OVRCam.GetComponent<Transform>().localEulerAngles.y,
                    OVRCam.GetComponent<Transform>().localEulerAngles.z);
            yield return new WaitForEndOfFrame();
        }
    }

    public void DisableCoastersUI()
    {
        foreach (GameObject attraction in GameObject.FindGameObjectsWithTag("LaserCollider"))
        {
            //Debug.Log(attraction.name);
            Transform background = attraction.transform.GetChild(0).transform.GetChild(0);
            background.GetComponent<RawImage>().enabled = false;
            background.GetComponent<BoxCollider>().enabled = false;
            background.transform.GetChild(0).GetComponent<RawImage>().enabled = false;
            background.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
            background.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().enabled = false;

            if (attraction.GetComponent<AttractionType>().attractionType == AttrType.roller_coaster)
            {
                background.transform.GetChild(1).GetComponent<RawImage>().enabled = false;
                background.transform.GetChild(1).GetComponent<BoxCollider>().enabled = false;
                background.transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().enabled = false;
            }
        }
    }

    public void SetDragPoint(GameObject sphere, GameObject arrow)
    {
        Plane hPlane = new Plane(Vector3.up, sphere.transform.position);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;

        Ray ray = new Ray(gunEnd.position, oculusRemoteScript.transform.forward);

        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            sphere.transform.position = new Vector3(ray.GetPoint(distance).x, sphere.transform.position.y, ray.GetPoint(distance).z);
            if(playerMoveScript.camMode == 0)
                arrow.transform.position = sphere.transform.position + new Vector3(0, 4, 0);
        }
    }

    public void SetDragPointArrow(GameObject arrow, GameObject sphere)
    {
        Plane hPlane = new Plane(-GameObject.Find("OVRPlayerController").transform.forward, arrow.transform.position);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;

        Ray ray = new Ray(gunEnd.position, oculusRemoteScript.transform.forward);

        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            arrow.transform.position = new Vector3(arrow.transform.position.x, ray.GetPoint(distance).y, arrow.transform.position.z);
            sphere.transform.position = arrow.transform.position - new Vector3(0, 4, 0);
        }
    }

    public Vector3 GetHittedPoint(GameObject torus)
    {
        Plane hPlane = new Plane(Vector3.up, torus.transform.position);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;

        Ray ray = new Ray(gunEnd.position, oculusRemoteScript.transform.forward);

        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            return ray.GetPoint(distance);
        }
        else
            return new Vector3(0,0,0);
    }

    float getYeuler(Vector3 forward)
    {
        Vector3 projectedForward = Vector3.ProjectOnPlane(forward, new Vector3(0, 1, 0));
        float angle = Vector3.Angle(projectedForward, new Vector3(1, 0, 0));
        /*if (projectedForward.z < 0)
        {
            angle = 180f + angle;
        }*/
        return angle;
    }

    public float GetDragTorusRotation(GameObject torus, Vector3 initPos)
    {
        Plane hPlane = new Plane(Vector3.up, torus.transform.position);
        // Plane.Raycast stores the distance from ray.origin to the hit point in this variable:
        float distance = 0;

        Ray ray = new Ray(gunEnd.position, oculusRemoteScript.transform.forward);

        

        // if the ray hits the plane...
        if (hPlane.Raycast(ray, out distance))
        {
            Vector3 center = torus.transform.position;
            Vector3 finalPos = ray.GetPoint(distance);

            Vector3 CI = Vector3.Normalize(initPos - center);
            Vector3 CF = Vector3.Normalize(finalPos - center);

            /*float scal = Vector3.Dot(CI, CF);

            float angle = Mathf.Acos(scal);

            return angle * 180 / Mathf.PI;*/

            /*Vector3 projectedForward = Vector3.ProjectOnPlane(forward, new Vector3(0, 1, 0));
            float angle = Vector3.Angle(projectedForward, new Vector3(1, 0, 0));
            if (projectedForward.z < 0)
            {
                angle = 180f + angle;
            }*/
            float angle = getYeuler(CF) - getYeuler(CI) + 360.0f;
            

            return angle;
        }
        return 0;
    }
}
