﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.VR;

public class PlayerMovement : MonoBehaviour
{
    [Header("Play Mode")]
    public bool oculus = false;
    public bool keyboard = true;

    [Header("Player parameters")]
    public bool canMove = true;
    public bool canRotate = true;
    public float playerSpeed;
    public float playerRotSpeed;
    public float gravity = -9.81f;
    public GameObject mainCamera;
    public int currentMode = 0;
    public bool flyMode = true;
    public GameObject forw;



    [Header("Current Roller Coaster")]
    public TextMeshProUGUI goInText;
    public bool canGoIn = false;
    public GameObject coaster;
    public GameObject playerTmpParent;
    public bool inAttraction = false;
    public AttrType attractType = 0;
    public bool attractionEventAvailable = false;

    [Header("Extern scripts")]
    public OculusRemote oculusRemoteScript;
    public UIManager uiScript;
    public CoasterEditor editor;

    [SerializeField]
    UnityEngine.XR.XRNode m_VRNode = UnityEngine.XR.XRNode.Head;

    [Header("Edit mode")]
    public bool isEditing = false;
    public int editMode = 0;    //0 : edition de nodes, 1 : add/suppr de nodes
    public int camMode = 0; //0 : Free Style, 1 : Top-Down

    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "RollerCoaster" && !inAttraction && !flyMode)
        {
            uiScript.onBuildUI = false;
            uiScript.showbuildingUI(uiScript.onBuildUI);

            AttrType type = other.gameObject.transform.root.GetChild(0).GetComponent<AttractionType>().attractionType;

            switch(type)
            {
                
                case AttrType.ferris_wheel:
                    uiScript.setPlayerInstructionText("Appuyer sur <sprite=6> pour faire un tour dans la grande roue");
                    break;
            }
            coaster = other.gameObject.transform.root.GetChild(0).gameObject;
            canGoIn = true;
            attractionEventAvailable = true;
            uiScript.enabledPlayerInstructionsText(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "RollerCoaster" && attractionEventAvailable)
        {
            attractionEventAvailable = false;
            uiScript.enabledPlayerInstructionsText(false);
        }
    }

    public void SwitchMode()
    {
        flyMode = !flyMode;
        if(!flyMode)
        {
            editor.DisableCoastersUI();
        }
        if(flyMode)
        {
            StartCoroutine(smoothModeFlip(new Vector3(0, 30, 0)));
            //playerSpeed = 5;
        } 
        else
        {
            StartCoroutine(smoothModeFlip(new Vector3(0, 0, 0)));
            //playerSpeed = 2;
        }
        uiScript.changeUIMode(flyMode);
    }

    public IEnumerator smoothModeFlip(Vector3 pos)
    {
        float t = 0f;

        Vector3 targetPos = pos;

        Vector3 initPos = transform.localPosition;

        while (t < 1.0f)
        {
            t += Time.deltaTime;
            transform.localPosition = Vector3.Lerp(initPos, targetPos, Mathf.SmoothStep(0.0f, 1.0f, t));
            yield return new WaitForEndOfFrame();
        }
    }

    public void SetCameraPosition(int index)
    {
        switch (index)
        {
            case 0:
                mainCamera.transform.localPosition = new Vector3(0f, 1f, 0f);
                break;

            case 1:
                mainCamera.transform.localPosition = new Vector3(0f, 0.4f, 0f);
                break;
        }
    }


    public void TPToAttraction()
    {
        canGoIn = false;
        inAttraction = true;
        canMove = false;
        attractionEventAvailable = false;
        uiScript.enabledPlayerInstructionsText(false);
        Vector3 pos = coaster.GetComponent<AttractionType>().GetStartPos();
        Transform newParent = coaster.GetComponent<AttractionType>().GetStartParent();
        attractType = coaster.GetComponent<AttractionType>().attractionType;
        transform.parent = newParent.transform;
        Vector3 test = coaster.GetComponent<AttractionType>().ChangeAttractionSpot(0);
        transform.localPosition = test;
        editor.transform.GetComponent<LineRenderer>().enabled = false;

        if (coaster.GetComponent<AttractionType>().attractionType == AttrType.roller_coaster)
        {
            transform.localPosition = new Vector3(0, -0.5f, 0);
            transform.localEulerAngles = new Vector3(180.0f, 180.0f, 0);
            coaster.transform.parent.GetComponent<CoasterAttraction>().resetWagon();
        }   

        //SetCameraPosition(1);
    }

    public void StartAttraction()
    {
        coaster.GetComponent<AttractionType>().StartAttr();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //GameObject previousBlock = selectedBlock;

        Vector3 playerMovement = new Vector3(0, 0, 0);
        Vector3 playerRotation = new Vector3(0, 0, 0);

        Vector3 beforePos = transform.localPosition;


        if (canMove || canRotate)    //If we didn't disabled player movement, we affect translation/rotation with arrow keys event
        {
            if (keyboard)    //If we play without oculus
            {
                if (canMove)
                {
                    if (Input.GetKey(KeyCode.UpArrow))
                        playerMovement = transform.forward + new Vector3(0, gravity, 0);

                    if (Input.GetKey(KeyCode.DownArrow))
                        playerMovement = -transform.forward / 2.0f + new Vector3(0, gravity, 0);
                }

                if (canRotate)
                {
                    if (Input.GetKey(KeyCode.RightArrow))
                        playerRotation = new Vector3(0, 1.5f, 0);
                    if (Input.GetKey(KeyCode.LeftArrow))
                        playerRotation = new Vector3(0, -1.5f, 0);
                }

            }
            else if (oculus)
            {
                float moveHorizontal = oculusRemoteScript.padPosition.x;
                float moveVertical = oculusRemoteScript.padPosition.y;

                /*var quaternion = UnityEngine.XR.InputTracking.GetLocalRotation(m_VRNode);
                var euler = quaternion.eulerAngles;
                //float moveHorizontal = 0.0f;
                if (euler.y < 180)
                    moveHorizontal = euler.y;
                else
                    moveHorizontal = euler.y - 360;*/

                playerMovement = forw.transform.forward * moveVertical;

                if(oculusRemoteScript.middlePadButtonIsDown)
                {
                    //if(!flyMode)
                        playerRotation += new Vector3(0, 1.5f * moveHorizontal, 0);
                    /*else
                    {
                        Vector3 dir = mainCamera.transform.forward;
                        dir.y = 0;
                        Vector3 target = mainCamera.transform.position + dir * 50.0f;

                        mainCamera.transform.RotateAround(target, Vector3.up, 30.0f * Time.deltaTime * -moveHorizontal);
                    }*/
                }
                    
                else
                    playerMovement += forw.transform.right * moveHorizontal;
            }
        }
        if (GetComponent<CharacterController>().isGrounded) //If the player hit the ground, gravity doesn't apply, to avoid entering the ground or other weird glitches
            playerMovement.y = 0.0f;

        transform.localPosition += playerMovement * Time.deltaTime * playerSpeed;
        transform.localEulerAngles += playerRotation * Time.deltaTime * playerRotSpeed;
        forw.transform.localEulerAngles += playerRotation * Time.deltaTime * playerRotSpeed;



        if (!canMove)
            transform.localPosition = beforePos;

        if (oculusRemoteScript.triggerPressed && canGoIn)
        {
            TPToAttraction();
            StartAttraction();
        }

        if(!flyMode)
        {
            bool canGo = false;
            GameObject[] attracs = GameObject.FindGameObjectsWithTag("RollerCoaster");

            foreach (GameObject attraction in attracs)
            {
                if (Vector3.Distance(attraction.transform.position, transform.position) < 10)
                {
                    canGo = true;
                    coaster = attraction.gameObject.transform.root.GetChild(0).gameObject; ;
                    break;
                }
            }

            if(canGo)
            {
                uiScript.enabledPlayerInstructionsText(true);
                uiScript.setPlayerInstructionText("Appuyer sur <sprite=6> pour monter dans la montagne russe");
                canGoIn = true;
            }
            else
            {
                uiScript.enabledPlayerInstructionsText(false);
                canGoIn = false;
            }
        }
        
        //if(inAttraction)
        //    transform.localEulerAngles = new Vector3(0, 0, 0);
    }
}
