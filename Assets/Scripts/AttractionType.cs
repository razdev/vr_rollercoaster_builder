﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum AttrType { roller_coaster, ferris_wheel, carousel }

public class AttractionType : MonoBehaviour {

    public AttrType attractionType;

    private Transform newParent;

	public Vector3 GetStartPos()
    {
        Vector3 pos = new Vector3(0, 0, 0);

        switch(attractionType)
        {
            case AttrType.roller_coaster:
                Transform root_rc = transform.root;
                GameObject wagon = root_rc.GetComponent<CoasterAttraction>().wagonPrefab;
                newParent = wagon.transform;
                pos = wagon.transform.position;
                break;

            case AttrType.ferris_wheel:
                Transform root = transform.root;
                Transform parent = root.transform.Find("Ferris Wheel/Wheel Base/Wheel Frame");
                pos = parent.transform.GetChild(0).transform.GetChild(0).transform.position;
                newParent = parent.transform.GetChild(0).transform.GetChild(0).transform;
                foreach (Transform wheel in parent)
                {
                    if (wheel.transform.GetChild(0).transform.GetChild(0).transform.position.y < pos.y)
                    {
                        pos = wheel.transform.GetChild(0).transform.GetChild(0).transform.position;
                        newParent = wheel.transform.GetChild(0).transform.GetChild(0).transform;
                    }
                }
                break;

            case AttrType.carousel:

                break;
        }

        return pos;
    }

    public void StartAttr()
    {
        switch (attractionType)
        {
            case AttrType.roller_coaster:

                break;

            case AttrType.ferris_wheel:
                Transform root = transform.root;
                root.transform.Find("Ferris Wheel/Wheel Base").GetComponent<FerrisSpin>().enabled = true;
                break;

            case AttrType.carousel:

                break;
        }
    }

    public Transform GetStartParent()
    {
        return newParent;
    }

    public Vector3 ChangeAttractionSpot(int spot)
    {
        Vector3 pos = new Vector3(0f, 0f, 0f);
        switch (attractionType)
        {
            case AttrType.roller_coaster:

                break;

            case AttrType.ferris_wheel:
                switch(spot)
                {
                    case 0:
                        pos = new Vector3(0f, -3.0f, 0f);
                        break;
                }

                Transform root = transform.root;
                root.transform.Find("Ferris Wheel/Wheel Base").GetComponent<FerrisSpin>().enabled = true;
                break;

            case AttrType.carousel:

                break;
        }
        return pos;
    }
}
