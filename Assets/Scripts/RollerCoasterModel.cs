﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Element
{
    public GameObject sqsq;
    public int indexElement;
    public int indexCoaster;
}

public class RollerCoasterModel : MonoBehaviour
{
    public GameObject sphere;
    public GameObject obj;

    public Element el;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void OnPointerOver(GameObject hit)
    {
        //if(hit.tag == "RollerCoaster")
        //    obj = Instantiate(sphere);
    }

    public void OnPointerExit(GameObject hit)
    {
        //if (hit.tag == "RollerCoaster")
            //obj.Delete;


    }

    public void rollercoaster()
    {

    }


    // Update is called once per frame
    void Update()
    {
        //Raycast from camera to mouse position, to get the eventually selected blocks
                Ray ray;
                RaycastHit hit;
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    if(hit.collider.gameObject.tag == "RollerCoaster")
                        obj = Instantiate(sphere);
                }
    }
}
