﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class OculusRemote : MonoBehaviour
{
    public bool oculus;

    public Vector2 padPosition;
    public bool middlePadButtonIsDown;
    public bool middlePadButtonPressed;
    public bool leftPadButtonIsDown;
    public bool rightPadButtonIsDown;

    public bool triggerIsDown;
    public bool triggerPressed;

    public bool backPressed;



    public Transform gunEnd; // reference to the gun's transform

    float distancePointLine(Vector3 linePos, Vector3 lineDir, Vector3 point)
    {
        lineDir.Normalize();
        Vector3 v = point - linePos;
        float d = Vector3.Dot(v, lineDir);
        Vector3 closestPoint = linePos + lineDir * d;
        return Vector3.Distance(point, closestPoint);
    }

    float pointSelectionScore(Vector3 linePos, Vector3 lineDir, Vector3 point)
    {
        return distancePointLine(linePos, lineDir, point) + Vector3.Distance(point, linePos);
    }

    float distancePointHorizontalTorus(Vector3 linePos, Vector3 lineDir, Vector3 center, float radius)
    { // get closest dist of torus
		float closest = 9999999.0f;
		for(float theta = 0.0f;theta < 2*3.14159265f ; theta += 0.1f)
		{
			Vector3 currentPoint = center + new Vector3(radius*Mathf.Sin(theta), 0.0f, radius*Mathf.Cos(theta));
			float currentScore = distancePointLine(linePos, lineDir, currentPoint);
			if(currentScore < closest)
			{
				closest = currentScore;
			}
		}
        return closest;
    }

    float horizontalTorusSelectionScore(Vector3 linePos, Vector3 lineDir, Vector3 center, float radius)
    { // get closest point of torus
		float closest = 9999999.0f;
		for(float theta = 0.0f;theta < 2*3.14159265f ; theta += 0.1f)
		{
			Vector3 currentPoint = center + new Vector3(radius*Mathf.Sin(theta), 0.0f, radius*Mathf.Cos(theta));
			float currentScore = pointSelectionScore(linePos, lineDir, currentPoint);
			if(currentScore < closest)
			{
				closest = currentScore;
			}
		}
        return 16.0f * closest;
    }

    float distancePointVerticalTorus(Vector3 linePos, Vector3 lineDir, Vector3 center, float radius, float angle)
    { // get closest dist of torus
		float closest = 9999999.0f;
		for(float theta = 0.0f;theta < 2*3.14159265f ; theta += 0.1f)
		{
			Vector3 displacement = new Vector3(radius*Mathf.Sin(theta), radius*Mathf.Cos(theta), 0.0f);
			displacement = Quaternion.AngleAxis(angle, new Vector3(0.0f, 1.0f, 0.0f)) * displacement;
			Vector3 currentPoint = center + displacement;
			float currentScore = distancePointLine(linePos, lineDir, currentPoint);
			if(currentScore < closest)
			{
				closest = currentScore;
			}
		}
        return closest;
    }

    float verticalTorusSelectionScore(Vector3 linePos, Vector3 lineDir, Vector3 center, float radius, float angle)
    { // get closest point of torus
		float closest = 9999999.0f;
		for(float theta = 0.0f;theta < 2*3.14159265f ; theta += 0.1f)
		{
			Vector3 displacement = new Vector3(radius*Mathf.Sin(theta), radius*Mathf.Cos(theta), 0.0f);
			displacement = Quaternion.AngleAxis(angle, new Vector3(0.0f, 1.0f, 0.0f)) * displacement;
			Vector3 currentPoint = center + displacement;;
			float currentScore = pointSelectionScore(linePos, lineDir, currentPoint);
			if(currentScore < closest)
			{
				closest = currentScore;
			}
		}
        return 16.0f * closest;
    }

    public GameObject getSelectedObject(GameObject[] list, float maxDist = 10f)
    {
        float minDist = maxDist + 1f;
        float minScore = 100000f * (maxDist + 1f);
        int minIndice = -1;
        for (int i = 0; i < list.Length; i++)
        {
            float dist;
			if(list[i].tag == "HorizontalTorus")
			{
				dist = distancePointHorizontalTorus(gunEnd.transform.position, transform.forward, list[i].transform.position, 2.0f);
			}
			else if(list[i].tag == "VerticalTorus")
			{
				dist = distancePointVerticalTorus(gunEnd.transform.position, transform.forward, list[i].transform.position, 2.0f, list[i].transform.parent.transform.eulerAngles.y);
			}
			else
			{
				dist = distancePointLine(gunEnd.transform.position, transform.forward, list[i].transform.position);
			}
			
            
            float score;
			if(list[i].tag == "HorizontalTorus")
			{
				score = horizontalTorusSelectionScore(gunEnd.transform.position, transform.forward, list[i].transform.position, 2.0f);
			}
			else if(list[i].tag == "VerticalTorus")
			{
				score = verticalTorusSelectionScore(gunEnd.transform.position, transform.forward, list[i].transform.position, 2.0f, list[i].transform.parent.transform.eulerAngles.y);
			}
			else
			{
				score = pointSelectionScore(gunEnd.transform.position, transform.forward, list[i].transform.position);
			}
			
            if (score < minScore && dist < maxDist)
            {
                minDist = dist;
                minScore = score;
                minIndice = i;
            }
        }
        if (minIndice == -1)
        {
            return null;
		}
        //Debug.Log("dist:" + minDist);
        return list[minIndice];
    }

    bool getTriggerIsDown()
    {
        if (oculus)
        {
            return OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
        }
        else
        {
            return Input.GetKeyDown(KeyCode.Z);
        }
    }

    bool getMiddlePadIsDown()
    {
        if (oculus)
        {
            return OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad);
        }
        else
        {
            return Input.GetKeyDown(KeyCode.A);
        }
    }

    Vector2 getTouchpadPosition()
    {
        return OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (oculus)
        {
            transform.forward = gunEnd.transform.forward;
        }
        else
        {
            if (Input.GetKey(KeyCode.O))
            {
                transform.forward = Quaternion.AngleAxis(-0.3f, new Vector3(1f, 0f, 0f)) * transform.forward;
            }
            if (Input.GetKey(KeyCode.K))
            {
                transform.forward = Quaternion.AngleAxis(-0.3f, new Vector3(0f, 1f, 0f)) * transform.forward;
            }
            if (Input.GetKey(KeyCode.L))
            {
                transform.forward = Quaternion.AngleAxis(0.3f, new Vector3(1f, 0f, 0f)) * transform.forward;
            }
            if (Input.GetKey(KeyCode.M))
            {
                transform.forward = Quaternion.AngleAxis(0.3f, new Vector3(0f, 1f, 0f)) * transform.forward;
            }
        }

        /*padPosition = getTouchpadPosition();

        if (!middlePadButtonIsDown)
        {
            middlePadButtonPressed = (triggerIsDown = getMiddlePadIsDown());
        }
        else
        {
            middlePadButtonIsDown = getMiddlePadIsDown();
            middlePadButtonPressed = false;
        }*/
        if(oculus)
        {
            padPosition = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);
            middlePadButtonPressed = OVRInput.GetDown(OVRInput.Button.PrimaryTouchpad);
            middlePadButtonIsDown = OVRInput.Get(OVRInput.Button.PrimaryTouchpad);
            triggerPressed = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger);
            triggerIsDown = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger);
            backPressed = OVRInput.GetDown(OVRInput.Button.Back);
            //leftPadButtonIsDown = OVRInput.Get(OVRInput.Button.Left);
            //rightPadButtonIsDown = OVRInput.Get(OVRInput.Button.Right);
        }
        else
        {
            if (Input.GetKey(KeyCode.RightArrow))
                padPosition.x = 1;
            if (Input.GetKey(KeyCode.LeftArrow))
                padPosition.x = -1;
            if(!(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow)))
                padPosition.x = 0;
            if (Input.GetKey(KeyCode.UpArrow))
                padPosition.y = 1;
            if (Input.GetKey(KeyCode.DownArrow))
                padPosition.y = -1;
            if (!(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow)))
                padPosition.y = 0;
            middlePadButtonPressed = Input.GetKeyDown(KeyCode.N);
            middlePadButtonIsDown = Input.GetKey(KeyCode.N);
            triggerPressed = Input.GetKeyDown(KeyCode.P);
            triggerIsDown = Input.GetKey(KeyCode.P);
            backPressed = Input.GetKeyDown(KeyCode.B);
            leftPadButtonIsDown = Input.GetKey(KeyCode.C);
            rightPadButtonIsDown = Input.GetKey(KeyCode.V);
        }
        

        /*if (!triggerIsDown)
        {
            triggerPressed = (triggerIsDown = getTriggerIsDown());
        }
        else
        {
            triggerIsDown = getTriggerIsDown();
            triggerPressed = false;
        }*/
    }
}