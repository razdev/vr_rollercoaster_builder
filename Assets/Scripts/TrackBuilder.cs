﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class CurveElementSelector {
	public CurveElement element;
	public GameObject selector;
	public float distance;
}

public class TrackBuilder : MonoBehaviour {
	
	public List<CurveElementSelector> curveElementSelectors;
	//public TrackElementSelector sphere;
	public GameObject selectorPrefab;
	public GameObject wagonPrefab;
	
	private GameObject headWagon;
	private Vector3 wagonPosition;
	
	
	public static float GetAngleVec2D(Vector2 p_vector2) {
		if (p_vector2.x < 0)
			return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
		return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;

	}
	
	void addElement(int index) {
		Debug.Log("add element at position "+index);
	}
	
	void deleteElement(int index) {
		Debug.Log("delete element from position "+index);
	}
	
	void moveElement(int index, Vector3 position) {
		Debug.Log("move element "+index);
	}
	
	void changeElementDirection(int index, Vector3 direction) {
		Debug.Log("change direction element "+index);
	}
	
	void changeElementLeft(int index, Vector3 left) {
		Debug.Log("change left element "+index);
	}
	
	Vector3 getWagonPosition(int section, float v) {
		Vector3 p;
		
		CurveElement previousElement = curveElementSelectors[section].element;
		CurveElement nextElement = curveElementSelectors[(section+1)%curveElementSelectors.Count].element;
		CurveElement prevPreviousElement = curveElementSelectors[(section+curveElementSelectors.Count-1)%curveElementSelectors.Count].element;
		CurveElement nextNextElement = curveElementSelectors[(section+2)%curveElementSelectors.Count].element;

		Vector3 p0, p1, p2, p3;
		p0 = previousElement.position;
		p1 = previousElement.position + previousElement.forward;
		p2 = nextElement.position - nextElement.forward;
		p3 = nextElement.position;
		p = p0*(1-v)*(1-v)*(1-v) + 3*p1*v*(1-v)*(1-v) + 3*p2*v*v*(1-v) + p3*v*v*v;
		
		return p ;
	}
	
	float getSegmentDistance(int section) {
		CurveElement previousElement = curveElementSelectors[section].element;
		CurveElement nextElement = curveElementSelectors[(section+1)%curveElementSelectors.Count].element;
		CurveElement prevPreviousElement = curveElementSelectors[(section+curveElementSelectors.Count-1)%curveElementSelectors.Count].element;
		CurveElement nextNextElement = curveElementSelectors[(section+2)%curveElementSelectors.Count].element;

		float distance = 0.0f;

		for(float t=0.0f;t<=1.0f;t+=0.2f) {
			distance += Vector3.Magnitude(getWagonPosition(section, t) - getWagonPosition(section, t+0.02f));
		}
		
		return distance;
	}

	// Use this for initialization
	void Start () {
		t = 0.0f;
		sec = 0;
	
		List<CurveElement> l = GetComponent<Track>().curve;
		curveElementSelectors.Clear();
		
		for(int i=0; i<l.Count; i++) {
			curveElementSelectors.Add(new CurveElementSelector());
			curveElementSelectors[i].element = l[i];
			curveElementSelectors[i].selector = Instantiate(selectorPrefab);
			selectorPrefab.transform.localPosition = l[i].position;
			curveElementSelectors[i].selector.GetComponent<TrackElementSelector>().setTrackBuilderAndGameObject(this, curveElementSelectors[i].selector);
		}
		
		for(int i=0; i<l.Count; i++) {
			curveElementSelectors[i].distance = getSegmentDistance(i);
		}
		
		headWagon = Instantiate(wagonPrefab);
		wagonPosition = curveElementSelectors[0].element.position;
		headWagon.transform.localPosition = wagonPosition;
	}
	
	private float t;
	private int sec;
	
	// Update is called once per frame
	void Update () {
		t += 0.005f / getSegmentDistance(sec);
		if(t > 1)
		{
			t -= 1.0f;
			sec++;
			if(sec >= curveElementSelectors.Count)
			{
				sec = 0;
			}
		}
		
		wagonPosition = getWagonPosition(sec, t);
		headWagon.transform.localPosition = transform.TransformPoint(wagonPosition);
	}
}
