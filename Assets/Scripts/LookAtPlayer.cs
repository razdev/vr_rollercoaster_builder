﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {

    public GameObject player;
    public Vector3 offset = new Vector3(0, 180, 0);

	// Use this for initialization
	void Start () {
        player = GameObject.Find("OVRPlayerController");
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(player.transform);
        transform.localEulerAngles = new Vector3(-transform.localEulerAngles.x + offset.x, transform.localEulerAngles.y + offset.y, transform.localEulerAngles.z + offset.z);
    }
}
